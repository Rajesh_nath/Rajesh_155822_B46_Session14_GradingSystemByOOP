<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Gathering Form</title>
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../resources/style.css" >

</head>
<body>
<div class="container">
<form method="post" action="process.php">
    <div class="studentInformation">
        <div class="form-group">
            <label for="name">Student's Name:</label>
        <input type="text" class="form-control" name="name" placeholder="Please Enter your Name" required>
        </div>
        <div class="form-group">
            <label for="studentId">Student's ID:</label>
            <input type="text" class="form-control" name="studentID" placeholder="Please Enter your Student ID" required>
        </div>
        <div class="form-group">
            <label for="markBangla">Bangla Mark:</label>
            <input type="number" step="any" min="0" max="100" class="form-control" name="markBangla"required placeholder="Please Enter Bangla Mark here...">
        </div>
        <div class="form-group">
            <label for="markEnglish">English Mark:</label>
            <input type="number" step="any" min="0" max="100" class="form-control" name="markEnglish" required placeholder="Please Enter English Mark here...">
        </div>
        <div class="form-group">
            <label for="markMath">Math Mark:</label>
            <input type="number" step="any" min="0" max="100" class="form-control" name="markMath" required placeholder="Please Enter Math Mark here...">
        </div>


    </div>
    <button type="submit" class="btn btn-primary btn-lg">Submit</button>

</form>
</div>

</body>
</html>