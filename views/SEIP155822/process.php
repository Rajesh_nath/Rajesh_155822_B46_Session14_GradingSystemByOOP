<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Gathering Form</title>
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../resources/style.css" >

</head>
<body>

<?php
require_once("../../vendor/autoload.php");

$objStudent = new \App\Student();
$objStudent->setName($_POST['name']);
$objStudent->setStudentID($_POST['studentID']);



$objCourse = new \App\Courses();

$objCourse->setMarkBangla($_POST["markBangla"]);
$objCourse->setGradeBangla();

$objCourse->setMarkEnglish($_POST["markEnglish"]);
$objCourse->setGradeEnglish();

$objCourse->setMarkMath($_POST["markMath"]);
$objCourse->setGradeMath();

$name = $objStudent->getName();
$studentID = $objStudent->getStudentID();
$markBangla = $objCourse->getMarkBangla();
$markMath = $objCourse->getMarkMath();
$markEnglish = $objCourse->getMarkEnglish();
$gradeBangla=$objCourse->getGradeBangla();
$gradeEnglish=$objCourse->getGradeEnglish();
$gradeMath=$objCourse->getGradeMath();


$resultPage = <<<RESULT
<table>
  <tr>
    <th>Name</th>
    <th>Student ID</th>
    <th>Bangla</th>
    <th>English</th>
    <th>Math</th>
  </tr>
  <tr>
    <td>$name</td>
    <td>$studentID</td>
    <td>$markBangla($gradeBangla)</td>
    <td>$markEnglish($gradeEnglish)</td>
    <td>$markMath($gradeMath)</td>
  </tr>
</table>

RESULT;


echo $resultPage;

file_put_contents("database.html",$resultPage,FILE_APPEND);

?>

</body>
</html>