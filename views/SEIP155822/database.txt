<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Gathering Form</title>
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../resources/style.css" >

</head>
<body>
<table>
  <tr>
    <th>Name</th>
    <th>Student ID</th>
    <th>Bangla</th>
    <th>English</th>
    <th>Math</th>
  </tr>
  <tr>
    <td>habib</td>
    <td>14525</td>
    <td>54(B)</td>
    <td>54(B)</td>
    <td>12(B)</td>
  </tr>
</table>
</body>
</html>